ActiveAdmin.register About do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :about,:mission, :vision,:about_image,:mission_image,:vision_image,:about_data,:vision_data,:mission_data
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
	index do
		id_column
		column :about_image do |img|
				if img.about_image.present?
			 	  image_tag "data:image/png;base64,#{Base64.encode64(img.about_data).gsub("\n", "")}",width: 70 ,height:70 
			 	else
			 		image_tag "aboutus.png" ,width: 50
			 	end
			end
			column :about
			
			
			column :mission_image do |img|
				if img.mission_image.present?
			 	  image_tag "data:image/png;base64,#{Base64.encode64(img.mission_data).gsub("\n", "")}",width: 70 ,height:70  
			 	else
			 		image_tag "mission.png" ,width: 50
			 	end
			end
			column :mission
			
			
			column :vision_image do |img|
				if img.vision_image.present?
			 	  image_tag "data:image/png;base64,#{Base64.encode64(img.vision_data).gsub("\n", "")}",width: 70 ,height:70   
			 	else
			 		image_tag "vision.png" ,width: 50
			 	end
			end
			column :vision
	  actions
	end

	show do
		attributes_table do 
			row :about_image do |img|
				if img.about_image.present?
			 	  image_tag "data:image/png;base64,#{Base64.encode64(img.about_data).gsub("\n", "")}",width:70  ,height:70 
			 	else
			 		image_tag "aboutus.png" ,width: 50
			 	end
			end
			row :about
			
			
			row :mission_image do |img|
				if img.mission_image.present?
			 	  image_tag "data:image/png;base64,#{Base64.encode64(img.mission_data).gsub("\n", "")}",width: 70 ,height:70  
			 	else
			 		image_tag "mission.png" ,width: 50
			 	end
			end
			row :mission
			
			
			row :vision_image do |img|
				if img.vision_image.present?
			 	  image_tag "data:image/png;base64,#{Base64.encode64(img.vision_data).gsub("\n", "")}",width: 70 ,height:70   
			 	else
			 		image_tag "vision.png" ,width: 50
			 	end
			end
			row :vision
		end
	end

	form do |f|
	    f.semantic_errors *f.object.errors.keys
	    f.inputs do
	    input :about_image, required: true, as: :file
	      input :about, as: :ckeditor
	      input :mission_image, required: true, as: :file
	      input :mission, as: :ckeditor
	      input :vision_image, required: true, as: :file
	      input :vision ,as: :ckeditor
	    end
	    actions
		end

	controller do
	  def create
	  	attr = permitted_params[:about]
		  @about = About.new
		  @about.about_data =  params[:about][:about_image].nil? ? nil : params[:about][:about_image].read
		  @about.about_image = params[:about][:about_image]
		  @about.mission_data =  params[:about][:mission_image].nil? ? nil : params[:about][:mission_image].read
		  @about.mission_image = params[:about][:mission_image]
		  @about.vision_data =  params[:about][:vision_image].nil? ? nil : params[:about][:vision_image].read
		  @about.vision_image = params[:about][:vision_image]
		  @about.about = params[:about][:about]
		  @about.mission = params[:about][:mission]
		  @about.vision = params[:about][:vision]
		  @about.save
		  flash[:notice] = "about created"
		  redirect_to "/admin/abouts"
	  end 

	  def update
	  	attr = permitted_params[:about]
	  	@find_about = About.find(params[:id])
	  	@about = @find_about.update(attr)
	  	if params[:about][:about_image].present?
	  		@find_about.update(about_data: params[:about][:about_image].read)
	  	end
	  	if params[:about][:mission_image].present?
	  		@find_about.update(mission_data: params[:about][:mission_image].read)
	  	end
	  	if params[:about][:vision_image].present?
	  		@find_about.update(vision_data: params[:about][:vision_image].read)
	  	end
	  	 flash[:notice] = "about updated"
	  	 redirect_to "/admin/abouts"
	  	end
	end 
end

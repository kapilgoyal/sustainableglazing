ActiveAdmin.register CompanyInfo do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :image,:data,:year,:project,:employer,:customer,:award,:description
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

index do
		id_column
		column "Image" do |img|
	 	  image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 50
	  end
	  column :project
	  column :year
	  column :employer
	  column :customer
	  column :award
	  column :description
	  actions
	end

	show do
		attributes_table do 
			row :image do |img|
				image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 100  
			end
			row :project
			row :year
			row :employer
			row :customer
			row :award
			row :description 

		end
	end

	form do |form|
		form.inputs do
	 	form.input :image,required: true, as: :file
	 	form.input :year
		form.input :project
		form.input :employer
		form.input :customer
		form.input :award
	 	form.input :description, :label => 'Description',as: :ckeditor

	 	end
	 	form.actions
	end

	controller do
	  def create
	  	attr = permitted_params[:company_info]
		  @company_info = CompanyInfo.new(year: attr[:year],project: attr[:project],employer: attr[:employer] ,customer: attr[:customer],award: attr[:award] )
		  @company_info.data =  params[:company_info][:image].read
		  @company_info.image = params[:company_info][:image]
		  @company_info.description = params[:company_info][:description]
		  @company_info.save
		  flash[:notice] = "company information created"
		  redirect_to "admin/company_infos"
	  end 

	  def update
	  	attr = permitted_params[:company_info]
		  @company_info = CompanyInfo.find(params[:id] )
		  @company_info = company_info.update(attr)
		  if params[:company_info][:image].present?
	  		@find_team.update(data: params[:company_info][:image].read)
	  		end
		  flash[:notice] = "company information updated"
		  redirect_to "admin/company_infos"
	  end 

	end 
end

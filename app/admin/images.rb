ActiveAdmin.register Image do
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
	permit_params :upload_image,:data
	#
	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end
	index do
		id_column
		# column :upload_image
		column "Image" do |img|
	 	  image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 50
	 	  # img.upload_image 
	    end
	  actions

	end

	show do
		attributes_table do 
			row :image do |img|
				image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 100  
			end
		end
		
	end

	form(:html => { :multipart => true }) do |f|
	    f.inputs "Images" do
	      f.input :upload_image,required: true, as: :file, input_html: { multiple: true }
		  end
	    f.actions
	end

	controller do
	  def create
	  	params[:image][:upload_image].each do |image|
		    @image = Image.new(upload_image: image)
		    @image.data = image.read
		    @image.save
		  end
		  flash[:notice] = "Uploaded"
		  redirect_to "/admin/images"
	  end 

	  def update
	  	@pre_image = Image.find(params[:id])
	  	@image = @pre_image.update(upload_image: params[:image][:upload_image].first ,data: params[:image][:upload_image].first.read)
	  	redirect_to "/admin/images"
	  end

	end 

end

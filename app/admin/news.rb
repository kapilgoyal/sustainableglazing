ActiveAdmin.register News do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :description, :project_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

	form do |form|
	   form.inputs do
	   form.input :title, :label => 'Title'
	   form.input :description, :label => 'Description'
	   form.input :project_id, :label => 'Project Name', :as => :select, :collection => Project.all.map{|u| [u.name,u.id]}
	    end
	   form.actions
	end




end

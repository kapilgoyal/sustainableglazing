ActiveAdmin.register ProjectCategory do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :name,:description
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
	form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      input :name
      input :description, as: :ckeditor
      
    end
    actions
  end
end

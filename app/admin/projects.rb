ActiveAdmin.register Project do
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
		permit_params :name, :description, :address,:project_type, :start_date, :end_date,
			:client, :consultant, :total_sq_m,:architect,:floor, image_ids: []

	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end


	index do
		id_column
		column :name
		column :description
		column :address
		column :project_type
		column :start_date
		column :end_date
		column "Image" do |project|
			panel "Project Images" do
    		attributes_table_for project.images do
 				 	row "Image" do |img|
			 	  	image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 70, height: 70
					end
    		end
    	end
		end
		actions
	end

	show do
		attributes_table do
      row :name
      row :description
      row :address
      row :client
      row :consultant
      row :total_sq_m
      row :architect
      row :floor
      row :project_type
      row :start_date 
      row :end_date       
    end

     panel "Project Images", only: :show do
      attributes_table_for project.images do
        row "Image" do |img|
			 	  image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 70 , height: 70
				end
      end
    end
	end
  
	form do |form|
	   form.inputs do
	   	# byebug
		 form.input :name, :label => 'Name'
		 form.input :description, :label => 'Description'
		 form.input :address, :label => 'Address'
	   	 form.input :client, :label => 'Client'
	   	 form.input :consultant, :label => 'Consultant'
	   	 form.input :total_sq_m, :label => 'Total sq. meter'
	   	 form.input :architect, :label => 'Architect'
	   	 form.input :floor, :label => 'Floor'
	   	 form.input :project_type, :label => 'Project Type', as: :select,collection: ProjectCategory.all.collect{ |u| [u.name, u.name] }
	   	 form.input :start_date, :label => 'Start_Date'
	     form.input :end_date, :label => 'End_Date'
	   	 form.input :images, input_html: { class: "select2" }, :label => 'Project Images',  :collection => Image.all.pluck(:upload_image,:id)
	   	 # form.input :images, input_html: { class: "select2" }, :label => 'Project Images',  :collection => Image.all.map{|img| [(image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}"),img.upload_image]}
	   
     end
		   form.actions
	end

	controller do
	  def destroy
	    destroy! do |format|
	      format.html { redirect_to admin_projects_path } if resource.valid?
	    end
	  end
	end

end

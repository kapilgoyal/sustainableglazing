ActiveAdmin.register Quote do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
	permit_params :name, :description, :email, :mob_no
	#
	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end
	form do |form|
	   	form.inputs do
		   form.input :name, :label => 'Name'
		   form.input :description, :label => 'Description', :as => :select, :collection => ["friendship","life","nature" "horoscope"], :include_blank => "I would like to disscuss about....."
	 	   form.input :email, :label => 'Email'
		   form.input :mob_no, :label => 'Phone Number'
		end
		   form.actions
	end

end

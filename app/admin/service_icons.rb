ActiveAdmin.register ServiceIcon do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :image, :data
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
	index do
		id_column
		# column :image
		column "ServiceIcon" do |img|
	 	  image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}"
	 	  # img.image 
	    end
	  actions

	end

	show do
		attributes_table do 
			row :image do |img|
				image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}"  
			end
		end
		
	end

	form(:html => { :multipart => true }) do |f|
	    f.inputs "ServiceIcon" do
	      f.input :image,required: true, as: :file, input_html: { multiple: true }
		  end
	    f.actions
	end

	controller do
	  def create
	  	params[:service_icon][:image].each do |image|
		    @image = ServiceIcon.new(image: image)
		    @image.data = image.read
		    @image.save
		  end
		  flash[:notice] = "Uploaded service icons"
		  redirect_to "/admin/service_icons"
	  end 

	  def update
	  	@pre_image = ServiceIcon.find(params[:id])
	  	@image = @pre_image.update(image: params[:service_icon][:image].first ,data: params[:service_icon][:image].first.read)
	  	redirect_to "/admin/service_icons"
	  end

	end 

end

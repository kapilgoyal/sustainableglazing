ActiveAdmin.register Service do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :title, :is_active, :description,:service_icon, service_icon_ids: []
#
# ort.string "title"
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

	index do
		id_column
		column :title
		column :is_active
		column :description
		column "Service Icon" do |service|
			panel "Service Icon" do
	    		attributes_table_for service.service_icons do
	 				 	row "Service Icon" do |img|
				 	  	image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}"
						end
	    		end
	    	end
    	end
		actions
	end

	show do
		attributes_table do
	      row :title
	      row :is_active
	      row :description
    	end

	     panel "Service Icon", only: :show do
		      attributes_table_for service.service_icons do
					row "Icon" do |img|
				 	  image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}"
					end
		      end
	    end
	end

	form do |form|
	   form.inputs do
	   form.input :title, :label => 'Title'
	   form.input :is_active
	   form.input :description, as: :ckeditor
	   form.input :service_icons, :label => 'service_icon', :as => :select, :collection => ServiceIcon.all.pluck(:image,:id)
	    end
	   form.actions
	end

end

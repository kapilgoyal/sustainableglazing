ActiveAdmin.register Team do
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
	permit_params :image, :name, :description,:data,:facebook ,:twitter, :google, :pinterest,:instagram
	#
	# or
	#
	# permit_params do
	#   permitted = [:permitted, :attributes]
	#   permitted << :other if params[:action] == 'create' && current_user.admin?
	#   permitted
	# end

	index do
		id_column
		column "Image" do |img|
	 	  image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 50
	  end
	  column :name
	  column :description
	  actions
	end

	show do
		attributes_table do 
			row :image do |img|
				image_tag "data:image/png;base64,#{Base64.encode64(img.data).gsub("\n", "")}",width: 100  
			end
			row :name 
			row :description 
			row :facebook
		 	row :twitter 
		 	row :google
		 	row :pinterest
		 	row :instagram 
		end
	end

	form do |form|
		form.inputs do
	 	form.input :image,required: true, as: :file
	 	form.input :name, :label => 'Name'
	 	form.input :description, :label => 'Description'
	 	form.inputs "Social link" do
	 	form.input :facebook
	 	form.input :twitter, :label => 'twitter'
	 	form.input :google, :label => 'google'
	 	form.input :pinterest, :label => 'pinterest'
	 	form.input :instagram, :label => 'instagram'

	 	end
	end
	 	form.actions
	end

	controller do
	  def create
	  	attr = permitted_params[:team]
		  @team = Team.new(facebook: attr[:facebook],twitter: attr[:twitter],google: attr[:google] ,instagram: attr[:instagram],pinterest: attr[:pinterest] )
		  @team.data =  params[:team][:image].read
		  @team.image = params[:team][:image]
		  @team.name = params[:team][:name]
		  @team.description = params[:team][:description]
		  @team.save
		  flash[:notice] = "team created"
		  redirect_to "/admin/teams"
	  end 

	  def update
	  	attr = permitted_params[:team]
	  	@find_team = Team.find(params[:id])
	  	@about = @find_team.update(attr)
	  	if params[:team][:image].present?
	  		@find_team.update(data: params[:team][:image].read)
	  	end
	  	 flash[:notice] = "team updated"
	  	 redirect_to "/admin/teams"
	  	end
	end 

end

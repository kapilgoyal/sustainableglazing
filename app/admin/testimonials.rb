ActiveAdmin.register Testimonial do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :description,:name,:client_image,:client_data, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
index do
	id_column
	column "Image" do |img|
	if img.client_image.present?
 	  image_tag "data:image/png;base64,#{Base64.encode64(img.client_data).gsub("\n", "")}",width: 50
 	else
 		image_tag "testimonal-img.png" ,width: 50
 	end
  end
  column :name
  column :description
  actions
end

show do
	attributes_table do 
		row :image do |img|
		if img.client_image.present?
 	  		image_tag "data:image/png;base64,#{Base64.encode64(img.client_data).gsub("\n", "")}",width: 60
		 	else
		 		image_tag "testimonal-img.png" ,width: 50
		 	end
		end
		row :name 
		row :description 

	end
	end
form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
    	input :name
    	input :client_image, :label => 'Image',as: :file
      	input :description, as: :ckeditor
      
    end
    actions
  end

  controller do
	  def create
	  	attr = permitted_params[:testimonial]
		  @testimonial = Testimonial.new(name: attr[:name],description: attr[:description])
		  @testimonial.client_data =  params[:testimonial][:client_image].read
		  @testimonial.client_image = params[:testimonial][:client_image]
		  @testimonial.save
		  flash[:notice] = "testimonials created"
		  redirect_to "admin/testimonials"
	  end 

	  def update
	  	@testimonial = Testimonial.find(params[:id])
	  	@image = @testimonial.update(name: params[:testimonial][:name] ,description: params[:testimonial][:description],client_data: params[:testimonial][:client_image]
.read)
	  	redirect_to "/admin/testimonials"
	  end

	end 

end

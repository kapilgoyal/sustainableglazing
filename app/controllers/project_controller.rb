class ProjectController < ApplicationController
  def index
  	@projects = Project.all
 	@project_categories = ProjectCategory.all
  end

  def show
  	@projects = Project.all
  	@project = Project.find(params[:id])
  	@project_images = @project.images
  	@outher_projects = @projects.where.not(id: @project.id).order('created_at DESC').limit(4)
  end
end

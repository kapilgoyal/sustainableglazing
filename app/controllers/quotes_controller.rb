class QuotesController < ApplicationController

	def create
		@quote = Quote.new(quote_params)
		respond_to do |format|
			if @quote.save
			  QuoteMailer.quote_email(@quote).deliver_now
			  flash[:success] = "Thank you! Your query has been submitted and our team will get back to you shortly."
			  format.js { render :file => "/welcome/company_update.js.erb" }
			else
			  flash.now[:error] = "Sorry! Your action is not working!"
			end
		end
	end

	private

	def quote_params
	  params.require(:quote).permit(:name, :description, :email, :mob_no)
	end       

end

class ServicesController < ApplicationController

  def solar_introduction
  end

  def solar_water_heater
  end

  def solar_street_light
  end

  def solar_power_plant
  end

  def solar_structure
  end

end

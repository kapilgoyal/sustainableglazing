class WelcomeController < ApplicationController
	
  def index
  	@quote = Quote.new
  	@projects = Project.all
  	@news = News.all
  	@teams = Team.all
  	@project_categories = ProjectCategory.all
    @testimonials = Testimonial.order("RANDOM()").limit(3)
    @services = Service.active.order("updated_at DESC")
    @company_info = CompanyInfo.all.order("updated_at DESC").last
    @company_heading = CompanyHeading.all.order("updated_at DESC").last
  end  


  def aboutus
    @aboutus = About.all.last
    @services = Service.all.order("updated_at DESC")
    @project_categories = ProjectCategory.order("RANDOM()").limit(3)
    @company_info = CompanyInfo.all.order("updated_at DESC").last
  	
  end

  def career
    
  end

  def company_update
    @company_update = CompanyUpdate.new(email: params[:company_update][:email])
    if @company_update.save
      flash[:success] = "Thanks for registering for News Letter"
    else
      flash.now[:error] = "Sorry! Your action is not working!"
    end
  end


end

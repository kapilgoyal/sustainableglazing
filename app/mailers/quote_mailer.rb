class QuoteMailer < ApplicationMailer
	def quote_email(quote)
		@quote = quote
		mail(to: "arpitmishra@sustainableglazing.com" ,
			from: @quote.email,
			subjects: "quote created") 
	end
end

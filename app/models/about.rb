class About < ApplicationRecord
	mount_uploader :about_image, AboutUploader
	mount_uploader :mission_image, AboutUploader
	mount_uploader :vision_image, AboutUploader
end

class News < ApplicationRecord

	def project_image
		if self.project_id.present?
			project = Project.find(self.project_id)
			pro_image = project.images.first.data
			return pro_image
		else
			return ""
		end	
	end
end

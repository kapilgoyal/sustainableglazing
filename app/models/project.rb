class Project < ApplicationRecord
has_many :news,:dependent => :delete_all
has_many :project_images
has_many :images, through: :project_images,:dependent => :delete_all
extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
end

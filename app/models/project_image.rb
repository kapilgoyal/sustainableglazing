class ProjectImage < ApplicationRecord
	mount_uploader :upload_image, ImageUploader	
	belongs_to :project
	belongs_to :image
end

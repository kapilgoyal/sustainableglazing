class Quote < ApplicationRecord
	validates :mob_no, :presence => {:message => 'number only 10 digit'},
                     :numericality => true,
                     :length => { :minimum => 10, :maximum => 15 }
end

class Service < ApplicationRecord
	has_many :service_images
	has_many :service_icons, through: :service_images,:dependent => :delete_all

  scope :active, -> {where(is_active: true)}

end
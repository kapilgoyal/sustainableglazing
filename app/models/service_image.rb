class ServiceImage < ApplicationRecord
	mount_uploader :image, ImageUploader	
	belongs_to :service
	belongs_to :service_icon
end

Rails.application.routes.draw do
  get 'services/solar_introduction'
  get 'services/solar_water_heater'
  get 'services/solar_street_light'
  get 'services/solar_power_plant'
  get 'services/solar_structure'


  get 'project/index'

  get 'welcome/index'
  root 'welcome#index'
  get 'welcome/aboutus'
  get 'welcome/career'
  get 'welcome/service'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users

 resources :quotes, only: [:create] do 
    # collection do
    #   post :quotes
    # end
  end 

  post 'welcome/company_update'
  resources :project, only:[:index,:show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

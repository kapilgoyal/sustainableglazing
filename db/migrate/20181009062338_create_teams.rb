class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.string :image
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end

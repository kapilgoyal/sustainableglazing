class CreateQuotes < ActiveRecord::Migration[5.1]
  def change
    create_table :quotes do |t|
      t.string :name
      t.string :description
      t.string :email
      t.integer :mob_no

      t.timestamps
    end
  end
end

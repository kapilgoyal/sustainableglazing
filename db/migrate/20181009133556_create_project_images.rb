class CreateProjectImages < ActiveRecord::Migration[5.1]
  def change
    create_table :project_images do |t|
      t.integer :project_id
      t.integer :image_id

      t.timestamps
    end
  end
end

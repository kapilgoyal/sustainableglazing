class AddclumnToprojects < ActiveRecord::Migration[5.1]
  def change
  	add_column :projects, :client, :string
  	add_column :projects, :consultant, :string
  	add_column :projects, :total_sq_m, :string
  	add_column :projects, :architect, :string
  	add_column :projects, :floor, :string
  	add_column :projects, :slug, :string, unique: true
  	
  end
end

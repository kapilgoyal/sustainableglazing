class AddcolumnToteam < ActiveRecord::Migration[5.1]
  def change
  	add_column :teams, :data, :binary
  	add_column :teams, :facebook, :string
  	add_column :teams, :twitter, :string
  	add_column :teams, :google, :string
  	add_column :teams, :pinterest, :string
  	add_column :teams, :instagram, :string
  end
end

class CreateCompanyUpdates < ActiveRecord::Migration[5.1]
  def change
    create_table :company_updates do |t|
      t.string :email

      t.timestamps
    end
  end
end

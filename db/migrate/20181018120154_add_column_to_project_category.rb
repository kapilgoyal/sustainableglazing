class AddColumnToProjectCategory < ActiveRecord::Migration[5.1]
  def change
  	add_column :project_categories, :description, :text
  end
end

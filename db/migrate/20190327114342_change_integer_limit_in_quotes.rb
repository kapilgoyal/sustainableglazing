class ChangeIntegerLimitInQuotes < ActiveRecord::Migration[5.1]
  def change
  	change_column :quotes, :mob_no, :integer, limit: 8
  end
end

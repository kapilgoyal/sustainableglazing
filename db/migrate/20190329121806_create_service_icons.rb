class CreateServiceIcons < ActiveRecord::Migration[5.1]
  def change
    create_table :service_icons do |t|
      t.string :image
      t.binary :data

      t.timestamps
    end
  end
end

class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.string :title
      t.text :description
      t.integer :service_icon
   
      t.timestamps
    end
  end
end

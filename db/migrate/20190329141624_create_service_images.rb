class CreateServiceImages < ActiveRecord::Migration[5.1]
  def change
    create_table :service_images do |t|
      t.integer :service_id
      t.integer :service_icon_id

      t.timestamps
    end
  end
end

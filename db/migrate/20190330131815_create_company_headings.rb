class CreateCompanyHeadings < ActiveRecord::Migration[5.1]
  def change
    create_table :company_headings do |t|
      t.text :color_title
      t.text :title
      t.text :sub_title

      t.timestamps
    end
  end
end

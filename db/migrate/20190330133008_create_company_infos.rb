class CreateCompanyInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :company_infos do |t|
      t.string :image
      t.binary :data
      t.string :year
      t.text :description
      t.string :project
      t.string :employer
      t.string :customer
      t.string :award

      t.timestamps
    end
  end
end

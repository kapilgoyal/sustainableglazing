class AddclumnToabout < ActiveRecord::Migration[5.1]
  def change
  	add_column :abouts, :about_image, :string
  	add_column :abouts, :mission_image, :string
  	add_column :abouts, :vision_image, :string
  	add_column :abouts, :about_data, :binary
  	add_column :abouts, :mission_data, :binary
  	add_column :abouts, :vision_data, :binary
  	add_column :testimonials, :client_image, :string
  	add_column :testimonials, :client_data, :binary

  end
end

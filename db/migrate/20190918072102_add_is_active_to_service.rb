class AddIsActiveToService < ActiveRecord::Migration[5.1]
  def change
    add_column :services, :is_active, :boolean, default: true
  end
end